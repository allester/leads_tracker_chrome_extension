const inputBtn = document.querySelector('#input-btn');
const inputEl = document.querySelector('#input-el');
let myLeads = [];
const ulEl = document.querySelector('#ul-el')
const deleteBtn = document.querySelector('#delete-btn')
const tabBtn = document.querySelector('#tab-btn')

deleteBtn.addEventListener('dblclick', clearLeads)

const leadsFromLocalStorage = JSON.parse( localStorage.getItem('myLeads') )

if (leadsFromLocalStorage) {
    myLeads = leadsFromLocalStorage
    render(myLeads)
}

tabBtn.addEventListener('click', getCurrentTabURL);

function getCurrentTabURL() {
    chrome.tabs.query({active: true, lastFocusedWindow: true}, function(tabs) {
        let url = tabs[0].url
        myLeads.push(url)
        
        // Save to localStorage
        localStorage.setItem("myLeads", JSON.stringify(myLeads))
    
        render(myLeads)
    })
}

function render(leads) {
    let listItems = ''
    for (let i = 0; i < leads.length; i++) {
        // First method
        listItems += `
            <li>
                <a href="https://${leads[i]}" target="_blank">
                    ${leads[i]} 
                </a>
            </li>`
    }
    ulEl.innerHTML = `${listItems}`
}

function clearLeads() {
    // Clear localStorage
    localStorage.clear()

    // Clear myLeads
    myLeads = []

    // Clear leads from page
    ulEl.innerHTML = ''

    render(myLeads)
}

inputBtn.addEventListener('click', saveLead)

function saveLead() {
    let inputValue = inputEl.value
    myLeads.push(inputValue)
    inputEl.value = ''

    localStorage.setItem("myLeads", JSON.stringify(myLeads))

    render(myLeads)

    // console.log( localStorage.getItem("myLeads") )
}

